*** Settings ***
Documentation    Cannot modify the account information when privacy and/or GDPR are not checked
...
...              This test case verifies that the account modification fails if the customer 
...              does not check the privacy and/or GDPR checkboxes, triggering an associated 
...              error message on the "MyIdentity" page.
Metadata         ID                           42
Metadata         Reference                    ACC_TC_005
Metadata         Automation priority          null
Metadata         Test case importance         Medium
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Cannot modify the account information when privacy and/or GDPR are not checked
    [Documentation]    Cannot modify the account information when privacy and/or GDPR are not checked

    &{dataset} =    Retrieve Dataset

    Given I created an account with gender "M" firstName "john" lastName "doe" email "johndoe@mail.com" password "pass1234" birthDate "01/01/1950" partnerOffers "yes" newsletter "yes"
    And I am logged in with email "johndoe@mail.com" and password "pass1234"
    And I am on the "MyIdentity" page
    When I fill MyIdentity fields with gender "${dataset}[gender]" firstName "${dataset}[first]" lastName "${dataset}[last]" email "${dataset}[mail]" oldPass "pass1234" newPass "${dataset}[password]" birthDate "${dataset}[birth]" partnerOffers "${dataset}[offers]" privacyPolicy "${dataset}[privacy]" newsletter "${dataset}[news]" gdpr "${dataset}[gdpr]" and submit
    Then I should still be on the "MyIdentity" page
    And An error message should be associated with the field "${dataset}[field]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_42_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_42_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_42_SETUP_VALUE} =    Get Variable Value    ${TEST_42_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_42_SETUP_VALUE is not None
        Run Keyword    ${TEST_42_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_42_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_42_TEARDOWN}.

    ${TEST_42_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_42_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_42_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_42_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                ${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=${city}    country=${country}    currency=${currency}

    ${gender} =      Get Test Param    DS_gender
    ${first} =       Get Test Param    DS_first
    ${last} =        Get Test Param    DS_last
    ${mail} =        Get Test Param    DS_mail
    ${password} =    Get Test Param    DS_password
    ${birth} =       Get Test Param    DS_birth
    ${offers} =      Get Test Param    DS_offers
    ${privacy} =     Get Test Param    DS_privacy
    ${news} =        Get Test Param    DS_news
    ${gdpr} =        Get Test Param    DS_gdpr
    ${field} =       Get Test Param    DS_field

    &{dataset} =    Create Dictionary    gender=${gender}        first=${first}    last=${last}        mail=${mail}
    ...                                  password=${password}    birth=${birth}    offers=${offers}    privacy=${privacy}
    ...                                  news=${news}            gdpr=${gdpr}      field=${field}

    RETURN    &{dataset}
