*** Settings ***
Documentation    Cannot write a customized message if the message is too long
...
...              This test case verifies that the system truncates customized messages to 
...              250 characters if the messages entered by the customer exceed this limit.
Metadata         ID                           50
Metadata         Reference                    CART_TC_005
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Cannot write a customized message if the message is too long
    [Documentation]    Cannot write a customized message if the message is too long

    &{docstrings} =    Retrieve Docstrings

    Given I am logged out
    And I am on the "Home" page
    When I navigate to category "accessoires"
    And I navigate to product "Mug personnalisable"
    And I customize with message "${docstrings}[docstring_1]"
    Then The customized message should be "${docstrings}[docstring_2]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_50_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_50_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_50_SETUP_VALUE} =    Get Variable Value    ${TEST_50_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_50_SETUP_VALUE is not None
        Run Keyword    ${TEST_50_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_50_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_50_TEARDOWN}.

    ${TEST_50_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_50_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_50_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_50_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Docstrings
    [Documentation]    Retrieves Squash TM's docstrings and stores them in a dictionary.
    ...
    ...                For instance, two docstrings have been defined in Squash TM,
    ...                the first one containing the string
    ...                "I am the
    ...                FIRST    docstring",
    ...                the second one containing the string "I am the second docstring"
    ...
    ...                First, this keyword retrieves values and converts them to an inline string :
    ...                ${docstring_1} =    Set Variable    I am the\nFIRST\tdocstring"
    ...
    ...                Then, this keyword stores the docstrings into the &{docstrings} dictionary
    ...                with each docstring name as key, and each docstring value as value :
    ...                ${docstrings} =    Create Dictionary    docstring_1=${docstring_1}    docstring_2=${docstring_2}

    ${docstring_1} =    Set Variable    Un très long message qui dépasse les 250 caractères maximum autorisés.\nUn très long message qui dépasse les 250 caractères maximum autorisés.\nUn très long message qui dépasse les 250 caractères maximum autorisés.\nUn très long message qui dépasse les 250 caractères maximum autorisés.
    ${docstring_2} =    Set Variable    Un très long message qui dépasse les 250 caractères maximum autorisés. Un très long message qui dépasse les 250 caractères maximum autorisés. Un très long message qui dépasse les 250 caractères maximum autorisés. Un très long message qui dépasse les

    &{docstrings} =    Create Dictionary    docstring_1=${docstring_1}    docstring_2=${docstring_2}

    RETURN    &{docstrings}
