*** Settings ***
Documentation    Cannot create an account when the password is too short
...
...              This test case verifies that the account creation fails if a customer 
...              provides a password that is too short, triggering an error message 
...              associated with the password field on the "AccountCreation" page.
Metadata         ID                           39
Metadata         Reference                    ACC_TC_002
Metadata         Automation priority          null
Metadata         Test case importance         Very high
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Cannot create an account when the password is too short
    [Documentation]    Cannot create an account when the password is too short

    Given I am on the "AccountCreation" page
    When I fill AccountCreation fields with gender "F" firstName "Niloofar" lastName "Norooz" password "word" email "niloofar@mail.com" birthDate "01/08/1993" acceptPartnerOffers "yes" acceptPrivacyPolicy "yes" acceptNewsletter "yes" acceptGdpr "yes" and submit
    Then I should still be on the "AccountCreation" page
    And An error message should be associated with the field "password_txt"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_39_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_39_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_39_SETUP_VALUE} =    Get Variable Value    ${TEST_39_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_39_SETUP_VALUE is not None
        Run Keyword    ${TEST_39_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_39_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_39_TEARDOWN}.

    ${TEST_39_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_39_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_39_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_39_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END
